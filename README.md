# Emotion Bots

This project is an experiment about organisms with simple loops of behaviour. Some of the middle stages of those loops can be understood as simplified emotions.

Emotions are defined here as variables that are affected by some parts of the organism state, and affects other parts of the organism state.


## Requirements to build the program

- C++ compiler (`sudo apt-get install build-essential`)
- opengl (`sudo apt-get install libgl1-mesa-dev`)
- cmake (`sudo apt-get install cmake`)
- conan (download [here](https://www.conan.io/downloads), recommended:
  `sudo apt-get install python-pip; sudo pip install setuptools wheel conan`)


## Compiling

Check that you have the requirements installed, and do the following:

Linux:

```
git clone https://bitbucket.org/jmmut/emotionbots.git
cd emotionbots
mkdir build && cd build
conan install --build missing ..
cmake -G "Unix Makefiles" ..
make
./bin/emotionbots
```

Windows:

```
git clone https://bitbucket.org/jmmut/emotionbots.git
cd emotionbots
conan install --build missing
cmake -G "Visual Studio 14 Win64"
cmake --build . --config Release
./bin/emotionbots.exe
```
## Running

### Controls:

You can click in bots, predators and the population graph to show more info.

keyboard: 

- space: start/stop computation
- f or +: increase iterations per second (affects bots mortality)
- v or -: decrease iterations per second (affects bots mortality)
- r: restart
- z: zoom out the population graph
- a: augment the zoom in the population graph
