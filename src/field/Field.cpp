/**
 * @file Field.cpp
 * @author jmmut
 * @date 2019-03-17.
 */

#include <complex>
#include <algorithm>
#include <utils/exception/StackTracedException.h>
#include "field/actions.h"
#include "Field.h"

using DistributionParameters = std::uniform_real_distribution<double>::param_type;

const double BOT_SIZE = 10;
static const int MAX_BOT_ENERGY = 100;

static const double FOOD_SIZE = 5.0;
static const double FOOD_ENERGY = 10.0;

Field::Field(const Size &size) : size(size),
        randomDevice{}, randomGenerator{randomDevice()}, uniformDistribution{0, 1} {
    restartBots();
    restartFoods();
}

void Field::restartBots() {
    bots.clear();
    for (size_t i = 0; i < 20; ++i) {
        bots.emplace_back(
                MAX_BOT_ENERGY,
                moveInRange(size.first / 10 * i, size.first),
                moveInRange(size.second / 1.618 * i, size.second),
                BOT_SIZE,
                BOT_SIZE);
    }

    predatorBots.clear();
    for (size_t i = 0; i < 3; ++i) {
        predatorBots.emplace_back(
                MAX_BOT_ENERGY,
                moveInRange(size.first / 1.618 * i, size.first),
                moveInRange(size.second / 10 * i, size.second),
                BOT_SIZE,
                BOT_SIZE);
    }
}

void Field::restartFoods() {
    foods.clear();
    for (size_t i = 0; i < 100; ++i) {
        generateOneFood();
    }
}

void Field::generateOneFood() {
    auto locationX = uniformDistribution(randomGenerator, DistributionParameters{0, size.first});
    auto locationY = uniformDistribution(randomGenerator, DistributionParameters{0, size.second});
    foods.emplace_back(FOOD_ENERGY, locationX, locationY, FOOD_SIZE, FOOD_SIZE);
}

void Field::iterate(Uint32 ms) {
    moveBots();
    feedBots(ms);
    ageBots(ms);
    reproduceBots(ms);
    generateFood(ms);
}

void Field::moveBots() {
    auto distributionParameters = DistributionParameters{0, 2 * M_PI};

    for (auto &bot : bots) {
        if (not bot.isDead()) {
            auto angle = uniformDistribution(randomGenerator, distributionParameters);
            auto speed = std::polar(bot.getActivation() * 10, angle);
            auto position = std::complex<double>{bot.getPosition().first, bot.getPosition().second} + speed;
            bot.setPosition(moveInFieldRange({position.real(), position.imag()}));
        }
    }
    for (auto &bot : predatorBots) {
        if (not bot.isDead()) {
            auto angle = uniformDistribution(randomGenerator, distributionParameters);
            auto speed = std::polar(bot.getActivation() * 10, angle);
            auto position = std::complex<double>{bot.getPosition().first, bot.getPosition().second} + speed;
            bot.setPosition(moveInFieldRange({position.real(), position.imag()}));
        }
    }
}

Position Field::moveInFieldRange(const Position &position) const {
    return {moveInRange(position.first, this->size.first), moveInRange(position.second, this->size.second)};
}

double Field::moveInRange(double position, double exclusiveLimit) const {
    if (exclusiveLimit == 0) {
        throw randomize::utils::exception::StackTracedException("can not use a exclusive limit of 0");
    }
    while (position < 0) {
        position += exclusiveLimit;
    }
    while (position >= exclusiveLimit) {
        position -= exclusiveLimit;
    }
    return position;
}

void Field::feedBots(double milliseconds) {
    for (auto &bot : bots) {
        for (auto &food : foods) {
            if (not bot.isDead() and canReach(bot, food)) {
                eat(bot, food, milliseconds);
            }
        }
    }
    for (auto &predatorBot : predatorBots) {
        for (auto &bot : bots) {
            if (not predatorBot.isDead() and canReach(predatorBot, bot)) {
                eat(predatorBot, bot, milliseconds);
            }
        }
    }
}

bool Field::canReach(LocatedEntity &entity1, LocatedEntity &entity2) {
    auto minXSize = std::min(entity1.getSize().first, entity2.getSize().first);
    auto minYSize = std::min(entity1.getSize().second, entity2.getSize().second);
    auto xDistance = std::abs(entity1.getPosition().first - entity2.getPosition().first);
    auto yDistance = std::abs(entity1.getPosition().second - entity2.getPosition().second);
    return xDistance < minXSize && yDistance < minYSize;
}

void Field::ageBots(double milliseconds) {
    for (auto &bot : bots) {
        if (not bot.isDead()) {
            bot.age(milliseconds);
        }
    }
    for (auto &bot : predatorBots) {
        if (not bot.isDead()) {
            bot.age(milliseconds);
        }
    }
}

void Field::reproduceBots(double milliseconds) {
    reproduceBots(bots, milliseconds);
    reproduceBots(predatorBots, milliseconds);
}

template<typename T>
void Field::reproduceBots(T &bots, double milliseconds) {
    T newBots;
    for (auto &bot : bots) {
        if (not bot.isDead()) {
            auto baby = bot.giveBirth();
            if (baby.has_value()) {
                newBots.push_back(BotEntity{
                        baby.value(),
                        bot.getPosition().first,
                        bot.getPosition().second,
                        bot.getSize().first,
                        bot.getSize().second});
            }
        }
    }
    bots.insert(bots.end(), newBots.begin(), newBots.end());
}

void Field::generateFood(double elapsedMilliseconds) {
    auto averageFoodPerMsPerSurface = 0.0000001;
    auto distributionParameters = DistributionParameters{0, 1 / averageFoodPerMsPerSurface};
    auto randomNumber = uniformDistribution(randomGenerator, distributionParameters);
    auto randomFoodRate = randomNumber + elapsedMilliseconds * size.first * size.second;
    auto foodsToCreate = randomFoodRate * averageFoodPerMsPerSurface;
    for (size_t i = 0; i < foodsToCreate; ++i) {
        generateOneFood();
    }
}

const Size &Field::getSize() const {
    return size;
}

void Field::setSize(const Size &size) {
    Field::size = size;
}

const std::vector<BotEntity> &Field::getBots() const {
    return bots;
}

const std::vector<FoodEntity> &Field::getFoods() const {
    return foods;
}

const std::vector<PredatorBotEntity> &Field::getPredatorBots() const {
    return predatorBots;
}

void Field::moveBotToFirstPosition(size_t botIndex) {
    std::swap(bots[botIndex], bots[0]);
}

void Field::movePredatorBotToFirstPosition(size_t botIndex) {
    std::swap(predatorBots[botIndex], predatorBots[0]);
}

void Field::removeDeadEntities(bool keepFirstBot, bool keepFirstPredator) {
    if (keepFirstBot) {
        removeDeadExceptTheFirst(bots, "bots");
    } else {
        removeDead(bots, "bots");
    }

    if (keepFirstPredator) {
        removeDeadExceptTheFirst(predatorBots, "predators");
    } else {
        removeDead(predatorBots, "predators");
    }

    removeDead(foods, "foods");
}

template<typename COLLECTION>
void Field::removeDeadExceptTheFirst(COLLECTION &entities, std::string name) {
    removeDeadAfterElement(entities, name, ++entities.begin());
}

template<typename COLLECTION>
void Field::removeDead(COLLECTION &entities, std::string name) {
    removeDeadAfterElement(entities, name, entities.begin());
}

template<typename COLLECTION, typename PTR_ELEMENT>
void Field::removeDeadAfterElement(COLLECTION &entities, std::string name, PTR_ELEMENT firstRemovableElement) {
    int removed = 0;
    size_t size_before = entities.size();
    auto newEnd = remove_if(firstRemovableElement, entities.end(), [&](auto &entity) {
        removed += entity.isDead();
        return entity.isDead();
    });
    while (entities.end() != newEnd) {
        entities.pop_back();
    }
    if (removed != 0) {
        LOG_DEBUG("removed %d %s, before there were %lu, now %lu", removed, name.c_str(), size_before, entities.size());
    }
}
