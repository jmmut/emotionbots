/**
 * @file BotEntity.h
 * @author jmmut
 * @date 2019-03-17.
 */

#ifndef EMOTIONBOTS_BOTENTITY_H
#define EMOTIONBOTS_BOTENTITY_H


#include "LocatedEntity.h"
#include "Bot.h"

class BotEntity;
using PredatorBotEntity = BotEntity;

class BotEntity : public Bot, public LocatedEntity {
public:
    BotEntity(double maxEnergy, double x, double y, double width, double height);
    BotEntity(Bot bot, double x, double y, double width, double height);

    ~BotEntity() override = default;

private:
};


#endif //EMOTIONBOTS_BOTENTITY_H
