/**
 * @file Bot.h
 * @author jmmut
 * @date 2019-03-17.
 */

#ifndef EMOTIONBOTS_BOT_H
#define EMOTIONBOTS_BOT_H


#include <string>
#include <sstream>
#include <ostream>
#include <optional>
#include <log/log.h>
#include <traits/Edible.h>
#include <core/utils.h>
#include "LocatedEntity.h"
#include "Personality.h"

static const double MILLISECONDS_TO_SECONDS_CONVERSION_COEF = 0.001;

static const int PREGNANCY_DURATION_SECONDS = 5;

static const int NOT_PREGNANT = -2;

static const double BIRTH_ENERGY_PENALTY_PER_MILLISECOND = 0.0001;

class Bot : public Edible {
public:
    Bot(double maxEnergy) : Bot{maxEnergy, 1.0, 0.0, 0.5, 0.8, Personality{10, 1}} {
    }

//    Bot(const Bot &copy) : Bot{copy.maxEnergy, copy.energy, copy.pain, copy.activation, copy.reproductionThreshold} {
//    }

    Bot(double maxEnergy, double energy, double pain, double activation, double reproductionThreshold,
            Personality personality)
            : maxEnergy(maxEnergy), energy(energy), pain(pain), activation(activation),
            reproductionThreshold(reproductionThreshold), secondsPregnant{NOT_PREGNANT}, personality{personality} {
    }

    ~Bot() override = default;

    void age(double milliseconds) {
        modifyEnergy(-((activation * 0.00005 + 0.000001) * milliseconds));

        pain -= 0.0001 * milliseconds;
        ensureInRange(pain);

        // high activation on mid energy, low energy otherwise: https://www.wolframalpha.com/input/?i=1+-+abs(2*x+-+1)+from+0+to+1
        activation = (1 - std::abs(2 * energy - 1)) * personality.explorationMotivation + pain;
        ensureInRange(activation);
        //        auto dtActivation = 0.1 * (0.25 - std::abs(energy - 0.5));
        //        activation += dtActivation;
//        activation = dampenToMiddleValue(activation);
//        pain = dampenToMiddleValue(pain);

        advancePregnancy(milliseconds);
    }

    void modifyEnergy(double increaseOrDecrease) {
        energy += increaseOrDecrease;
        ensureInRange(energy);
    }

    void advancePregnancy(double milliseconds) {
        if (isPregnant()) {
            secondsPregnant += milliseconds * MILLISECONDS_TO_SECONDS_CONVERSION_COEF;
            modifyEnergy(-BIRTH_ENERGY_PENALTY_PER_MILLISECOND * milliseconds);
        } else {
            if (energy > reproductionThreshold) {
                secondsPregnant = 0;
            }
        }
    }

    std::optional<Bot> giveBirth() {
        if (secondsPregnant > PREGNANCY_DURATION_SECONDS) {
            secondsPregnant = NOT_PREGNANT;
            return Bot{maxEnergy, 0.5, 0.0, 0.5, reproductionThreshold, personality.mutate()};
        } else {
            return std::nullopt;
        }
    }

    bool isPregnant() const {
        return secondsPregnant > -1;
    }

    void eat(double energyEaten) {
        energy += energyEaten / maxEnergy;
        ensureInRange(energy);
    }

    double beEaten(double energyEaten) override {
        auto previousEnergy = energy;
        energy -= energyEaten / maxEnergy;
        energy = inRange(energy);
        auto energyDifference = previousEnergy - energy;
        pain += energyDifference * personality.painSensitivity;
        ensureInRange(pain);
        return energyDifference * maxEnergy;
    }

    void ensureInRange(double &value) {
        if (value < 0) {
            value = 0;
        } else if (value > 1) {
            value = 1;
        }
    }

    bool isDead () const {
        return energy <= 0;
    }

    double getEnergy() const {
        return energy * maxEnergy;
    }

    double getEnergyRatio() const {
        return energy;
    }

    double getActivation() const {
        return activation;
    }

    double getPain() const {
        return pain;
    }

    const Personality &getPersonality() const {
        return personality;
    }

    std::string toString() const {
        std::stringstream ss;
        ss << *this;
        return ss.str();
    }

    friend std::ostream &operator<<(std::ostream &os, const Bot &bot) {
        os << " pain: " << bot.pain << " energy: " << bot.energy << " activation: " << bot.activation
                << " personality:{" << bot.personality << "}";
        if (bot.isPregnant()) {
            os << " pregnant for " << bot.secondsPregnant << " seconds";
        }
        return os;
    }

private:
    double dampenToMiddleValue(double value) {
        return value + (value - 0.5) * 0.01;
    }

    double maxEnergy;
    double energy; // 0 to 1

    double pain;    // 0 to 1
    double activation;  // 0 to 1

    double reproductionThreshold;  // 0 to 1
    double secondsPregnant;

    Personality personality;
};


#endif //EMOTIONBOTS_BOT_H
