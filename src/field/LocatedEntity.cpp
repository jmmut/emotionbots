/**
 * @file LocatedEntity.cpp
 * @author jmmut
 * @date 2019-03-17.
 */

#include "LocatedEntity.h"

LocatedEntity::LocatedEntity(double x, double y, double width, double height)
        : x(x), y(y), width(width), height(height) {
}

Position LocatedEntity::getPosition() const {
    return {x, y};
}

void LocatedEntity::setPosition(Position position) {
    x = position.first;
    y = position.second;
}

Size LocatedEntity::getSize() const {
    return {width, height};
}

void LocatedEntity::setSize(Size size) {
    width = size.first;
    height = size.second;
}
