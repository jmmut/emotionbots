/**
 * @file Field.h
 * @author jmmut
 * @date 2019-03-17.
 */

#ifndef EMOTIONBOTS_FIELD_H
#define EMOTIONBOTS_FIELD_H

#include <vector>
#include <random>
#include <SDL_stdinc.h>
#include <complex>
#include "LocatedEntity.h"
#include "BotEntity.h"
#include "FoodEntity.h"

class Field {
public:
    Field(const Size &size);

    void iterate(Uint32 ms);

    const Size &getSize() const;
    void setSize(const Size &size);

    const std::vector<BotEntity> &getBots() const;
    const std::vector<FoodEntity> &getFoods() const;
    const std::vector<PredatorBotEntity> &getPredatorBots() const;

    void generateFood(double elapsedMilliseconds);
    void restartBots();
    void restartFoods();

    void moveBotToFirstPosition(size_t botIndex);
    void movePredatorBotToFirstPosition(size_t botIndex);

    void removeDeadEntities(bool keepFirstBot, bool keepFirstPredator);

private:
    Size size;
    std::vector<BotEntity> bots;
    std::vector<PredatorBotEntity> predatorBots;
    std::vector<FoodEntity> foods;

    std::random_device randomDevice;  //Will be used to obtain a seed for the random number engine
    std::mt19937 randomGenerator; //Standard mersenne_twister_engine seeded with randomDevice()
    std::uniform_real_distribution<> uniformDistribution;

    void moveBots();
    Position moveInFieldRange(const Position &position) const;
    double moveInRange(double position, double exclusiveLimit) const;

    void ageBots(double milliseconds);
    void reproduceBots(double milliseconds);
    void feedBots(double milliseconds);
    bool canReach(LocatedEntity &entity1, LocatedEntity &entity2);
    void generateOneFood();

    template<typename COLLECTION>
    void removeDeadExceptTheFirst(COLLECTION &entities, std::string name);

    template<typename COLLECTION>
    void removeDead(COLLECTION &entities, std::string name);

    template<typename COLLECTION, typename PTR_ELEMENT>
    void removeDeadAfterElement(COLLECTION &entities, std::string name, PTR_ELEMENT firstRemovableElement);

    template<typename T>
    void reproduceBots(T &bots, double milliseconds);
};

#endif //EMOTIONBOTS_FIELD_H
