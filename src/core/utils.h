/**
 * @file utils.h
 * @author jmmut
 * @date 2019-03-21.
 */

#ifndef EMOTIONBOTS_UTILS_H
#define EMOTIONBOTS_UTILS_H

inline double inRange(double value) {
    if (value < 0) {
        return 0;
    } else if (value > 1) {
        return 1;
    } else {
        return value;
    }
}

#endif //EMOTIONBOTS_UTILS_H
