/**
 * @file Edible.h
 * @author jmmut
 * @date 2019-03-21.
 */

#ifndef EMOTIONBOTS_EDIBLE_H
#define EMOTIONBOTS_EDIBLE_H

class Edible {
public:
    virtual ~Edible() = default;
    virtual double beEaten(double energyEaten) = 0;
};
#endif //EMOTIONBOTS_EDIBLE_H
