
#include "ui/Manager.h"
#include "ui/Window2D.h"

int main(int argc, char **argv) {
    randomize::log::log_level = argc == 2 ? randomize::log::parseLogLevel(argv[1]) : LOG_INFO_LEVEL;

    Dispatcher dispatcher;
    dispatcher.addEventHandler([&dispatcher](SDL_Event &event) { dispatcher.endDispatcher(); },
            [](SDL_Event &event) {
                return event.type == SDL_QUIT || (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE);
            }
    );

    auto window = std::make_shared<Window2D>();
    window->open();
    dispatcher.addEventHandler([&dispatcher, &window](SDL_Event &event) { window->close(); },
            [windowID = window->getWindowID()](SDL_Event &event) {
                return event.type == SDL_WINDOWEVENT
                        && event.window.windowID == windowID
                        && event.window.event == SDL_WINDOWEVENT_CLOSE;
            }
    );

    Manager manager(dispatcher, window);
    manager.activate();

    dispatcher.startDispatcher();

    exit(0);
    return 0;
}

