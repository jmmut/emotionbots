/**
 * @file Manager.cpp
 * @author jmmut
 * @date 2018-05-27.
 */

#include "Manager.h"
#include "utils/exception/StackTracedException.h"
#include "Window2D.h"
#include "Drawer.h"

using namespace std::string_literals;

static const double HORIZONTAL_DIVISION = 0.6;
static const double LEFT_DIVISION = 0.25;
static const double RIGHT_DIVISION = 0.75;

static const Position FIELD_POSITION_COEF = {LEFT_DIVISION, 0};
static const Size FIELD_SIZE_COEF = {RIGHT_DIVISION - LEFT_DIVISION, HORIZONTAL_DIVISION};

static const Position STATS_POSITION_COEF = {RIGHT_DIVISION, 0};
static const Size STATS_SIZE_COEF = {1 - RIGHT_DIVISION, HORIZONTAL_DIVISION};

static const Position POPULATION_STATS_POSITION_COEF = {LEFT_DIVISION, HORIZONTAL_DIVISION};
static const Size POPULATION_STATS_SIZE_COEF = {RIGHT_DIVISION - LEFT_DIVISION, 1 - HORIZONTAL_DIVISION};

static const Position PREDATOR_STATS_POSITION_COEF = {0, 0};
static const Size PREDATOR_STATS_SIZE_COEF = {LEFT_DIVISION, HORIZONTAL_DIVISION};

static const Position PERSONALITY_STATS_POSITION_COEF = {RIGHT_DIVISION, HORIZONTAL_DIVISION};
static const Size PERSONALITY_STATS_SIZE_COEF = {1 - RIGHT_DIVISION, 1 - HORIZONTAL_DIVISION};


Manager::Manager(Dispatcher &d, std::shared_ptr<Window> w)
        : MediumManagerBase(d, w),
        field{{getRectFromCoef(FIELD_POSITION_COEF, FIELD_SIZE_COEF).w,
                getRectFromCoef(FIELD_POSITION_COEF, FIELD_SIZE_COEF).h}},
        populations{{{}, BOT_COLOR}, {{}, PREDATOR_COLOR}, {{}, FOOD_COLOR}} {
}

void Manager::onStep(Uint32 ms) {
    MediumManagerBase::onStep(ms);

    auto fieldFrame = getRectFromCoef(FIELD_POSITION_COEF, FIELD_SIZE_COEF);
    field.setSize({fieldFrame.w, fieldFrame.h});
    field.iterate(ms);

    field.removeDeadEntities(isAnyBotSelected(), isAnyPredatorSelected());

    updatePopulationStats();
}

bool Manager::isAnyBotSelected() const { return lastSelectedBot != -1; }

bool Manager::isAnyPredatorSelected() const { return lastSelectedPredator != -1; }

void Manager::updatePopulationStats() {
    populations[0].first.push_front(field.getBots().size());
    populations[1].first.push_front(field.getPredatorBots().size());
    populations[2].first.push_front(field.getFoods().size());
    for (auto &population : populations) {
        if (population.first.size() > 100000) {
            population.first.pop_back();
        }
    }
}

void Manager::onDisplay(Uint32 ms) {
    auto &window = tryGetWindow();
    window.renderClearBlack();

    auto drawer = Drawer{window};

    drawer.drawField(field, getRectFromCoef(FIELD_POSITION_COEF, FIELD_SIZE_COEF));

    if (isAnyBotSelected()) {
        const auto &bot = field.getBots()[lastSelectedBot];
        if (titleSelected == TitleSelection::BOT) {
            tryGetWindow().setTitle("bot status: " + bot.toString());
        }
        drawer.drawBotStats(bot, getRectFromCoef(STATS_POSITION_COEF, STATS_SIZE_COEF));
    }

    if (isAnyPredatorSelected()) {
        const auto &bot = field.getPredatorBots()[lastSelectedPredator];
        if (titleSelected == TitleSelection::PREDATOR) {
            tryGetWindow().setTitle("predator bot status: " + bot.toString());
        }
        drawer.drawBotStats(bot, getRectFromCoef(PREDATOR_STATS_POSITION_COEF, PREDATOR_STATS_SIZE_COEF));
    }

    drawer.drawPopulationStats(populations, getRectFromCoef(POPULATION_STATS_POSITION_COEF, POPULATION_STATS_SIZE_COEF),
            10.0, timeScale);
    if (titleSelected == TitleSelection::POPULATION) {
        tryGetWindow().setTitle("population: " + std::to_string(field.getBots().size()) + " bots, "
                + std::to_string(field.getPredatorBots().size()) + " predators, "
                + std::to_string(field.getFoods().size()) + " foods");
    }

    drawer.drawPersonalityStats(field.getBots(),
            getRectFromCoef(PERSONALITY_STATS_POSITION_COEF, PERSONALITY_STATS_SIZE_COEF));
    
    window.renderPresent();
}

Window2D &Manager::tryGetWindow() const {
    auto ptr = this->window.lock();
    if (ptr == nullptr) {
        throw randomize::utils::exception::StackTracedException("Window unavailable");
    } else {
        return dynamic_cast<Window2D &>(ptr.operator*());
    }
}

SDL_Rect Manager::getRect(const Position &position, const Size &size) const {
    return getRectFromCoef(1, 1, position, size);
}

SDL_Rect Manager::getRectFromCoef(const Position &position, const Size &size) const {
    auto &window = tryGetWindow();
    auto width = 0, height = 0;
    window.getWindowSize(width, height);
    return getRectFromCoef(width, height, position, size);
}

SDL_Rect Manager::getRectFromCoef(int width, int height, const Position &position, const Size &size) const {
    SDL_Rect rect{
            int(width * position.first), int(height * position.second),
            int(width * size.first), int(height * size.second)};
    return rect;
}

void Manager::onKeyChange(SDL_KeyboardEvent &event) {
    MediumManagerBase::onKeyChange(event);
    if (event.type == SDL_KEYUP) {
        return;
    }
    switch (event.keysym.sym) {
    case SDLK_SPACE:
        if (stepActive) {
            stopOnStep();
        } else {
            startOnStep();
        }
        break;
    case SDLK_PLUS:
    case SDLK_KP_PLUS:
    case SDLK_f:
        setStepFPS(getStepFPS() + 1);
        tryGetWindow().setTitle("FPS set to: "s + std::to_string(getStepFPS()));
        titleSelected = TitleSelection::NOTHING;
        break;
    case SDLK_MINUS:
    case SDLK_KP_MINUS:
    case SDLK_v:
        setStepFPS(std::max(getStepFPS() - 1, 1u));
        tryGetWindow().setTitle("FPS set to: "s + std::to_string(getStepFPS()));
        titleSelected = TitleSelection::NOTHING;
        break;
    case SDLK_r:
        field.restartBots();
        field.restartFoods();
        break;
    case SDLK_z:
        timeScale /= 2;
        break;
    case SDLK_a:
        timeScale *= 2;
        break;
    default:
        break;
    }
}

void Manager::onMouseButton(SDL_MouseButtonEvent &event) {
    if (event.type == SDL_MOUSEBUTTONUP) {
        return;
    }

    auto populationStatsRect = getRectFromCoef(POPULATION_STATS_POSITION_COEF, POPULATION_STATS_SIZE_COEF);
    if (insideRect(populationStatsRect, event.x, event.y)) {
        titleSelected = TitleSelection::POPULATION;
        return;
    }
    auto personalityStatsRect = getRectFromCoef(PERSONALITY_STATS_POSITION_COEF, PERSONALITY_STATS_SIZE_COEF);
    if (insideRect(personalityStatsRect, event.x, event.y)) {
        titleSelected = TitleSelection::PERSONALITY;
        PersonalityConverter converter;
        auto values = converter.toValue(personalityStatsRect, {event.x, event.y});
        std::stringstream ss;
        ss << "personality: " << Personality{values.first, values.second};
        tryGetWindow().setTitle(ss.str());
        return;
    }

    auto fieldPosition = getPositionFromCoefficients(FIELD_POSITION_COEF);
    for (size_t i = 0; i < field.getBots().size(); ++i) {
        auto &bot = field.getBots()[i];
        auto position = bot.getPosition();
        auto size = bot.getSize();
        if (insideRect(getRect(fieldPosition + position, size), event.x, event.y)) {
            if (event.button == 1) {
                field.moveBotToFirstPosition(i);
                lastSelectedBot = 0;
                titleSelected = TitleSelection::BOT;
            } else {
                lastSelectedBot = -1;
                titleSelected = TitleSelection::NOTHING;
            }
            return;
        }
    }
    for (size_t i = 0; i < field.getPredatorBots().size(); ++i) {
        auto &bot = field.getPredatorBots()[i];
        auto position = bot.getPosition();
        auto size = bot.getSize();
        if (insideRect(getRect(fieldPosition + position, size), event.x, event.y)) {
            if (event.button == 1) {
                field.movePredatorBotToFirstPosition(i);
                lastSelectedPredator = 0;
                titleSelected = TitleSelection::PREDATOR;
            } else {
                lastSelectedPredator = -1;
                titleSelected = TitleSelection::NOTHING;
            }
            return;
        }
    }
}

Position Manager::getPositionFromCoefficients(const Position &positionCoef) {
    auto &window = tryGetWindow();
    auto width = 0, height = 0;
    window.getWindowSize(width, height);
    return {width * positionCoef.first, height * positionCoef.second};
}

bool Manager::insideRect(SDL_Rect rect, Sint32 testX, Sint32 testY) const {
    return testX >= rect.x
            && testX < (rect.x + rect.w)
            && testY >= rect.y
            && testY < (rect.y + rect.h);
}

