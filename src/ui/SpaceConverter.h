/**
 * @file SpaceConverter.h
 * @author jmmut
 * @date 2019-05-09.
 */

#ifndef EMOTIONBOTS_SPACECONVERTER_H
#define EMOTIONBOTS_SPACECONVERTER_H


#include <SDL2/SDL_rect.h>

class SpaceConverter {
public:
    virtual ~SpaceConverter() = default;

    virtual Position toPixel(SDL_Rect frame, Position value) = 0;
    virtual Position toValue(SDL_Rect frame, Position pixel) = 0;
};

class PersonalityConverter : public SpaceConverter {
private:
    static constexpr double scale = 10;
public:
    Position toPixel(SDL_Rect frame, Position value) override {
        auto logPain = log(value.first);
        auto logExplorer = log(value.second);
        auto locationX = frame.x + frame.w / 2.0 + logPain * scale;
        auto locationY = frame.y + frame.h / 2.0 - logExplorer * scale;
        return {locationX, locationY};
    }

    Position toValue(SDL_Rect frame, Position pixel) override {
        auto logPain = (pixel.first - frame.x - frame.w / 2.0) / scale;
        auto logExplorer = - (pixel.second - frame.y - frame.h / 2.0) / scale;
        auto pain = exp(logPain);
        auto explorer = exp(logExplorer);
        return {pain, explorer};
    }
};

#endif //EMOTIONBOTS_SPACECONVERTER_H
