/**
 * @file Manager.h
 * @author jmmut
 * @date 2018-05-27.
 */

#ifndef MYTEMPLATEPROJECT_MANAGER_H
#define MYTEMPLATEPROJECT_MANAGER_H

#include <field/Field.h>
#include <deque>
#include "events/Dispatcher.h"
#include "events/EventCallThis.h"
#include "manager_templates/MediumManagerBase.h"
#include "Window2D.h"
#include "field/Bot.h"
#include "field/BotEntity.h"
#include "Drawer.h"

class Manager: public MediumManagerBase {
public:
    Manager(Dispatcher &d, std::shared_ptr<Window> w);

private:
    Field field;
    long lastSelectedBot = -1;
    long lastSelectedPredator = -1;
    double timeScale = 0.1;

    enum class TitleSelection {
        NOTHING, POPULATION, PERSONALITY, PREDATOR, BOT
    };
    TitleSelection titleSelected = TitleSelection::NOTHING;
    std::vector<std::pair<std::deque<long>, Color>> populations;

    void onDisplay( Uint32 ms) override;
    void onKeyChange(SDL_KeyboardEvent &event) override;
    void onStep(Uint32 ms) override;
    void onMouseButton(SDL_MouseButtonEvent &event) override;

    Window2D &tryGetWindow() const;

    bool insideRect(SDL_Rect rect, Sint32 testX, Sint32 testY) const;
    SDL_Rect getRect(const Position &position, const Size &size) const;
    SDL_Rect getRectFromCoef(const Position &position, const Size &size) const;
    SDL_Rect getRectFromCoef(int width, int height, const Position &position, const Size &size) const;
    Position getPositionFromCoefficients(const Position &positionCoef);
    void updatePopulationStats();

    bool isAnyBotSelected() const;
    bool isAnyPredatorSelected() const;
};


#endif //MYTEMPLATEPROJECT_MANAGER_H
