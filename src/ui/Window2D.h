/**
 * @file Manager.h
 * @author jmmut
 * @date 2018-05-27.
 */

#ifndef MYTEMPLATEPROJECT_WINDOW_H
#define MYTEMPLATEPROJECT_WINDOW_H

#include <random>
#include <chrono>
#include <complex>
#include <graphics/Window.h>

struct Color {
    Uint8 red, green, blue, alpha;
};

class Window2D : public WindowRenderer {
public:
    explicit Window2D(Uint32 flags = 0) : WindowRenderer{flags} {}
    ~Window2D() override = default;

    void open() override;
    void fillRect(Color color, SDL_Rect &rect);
    void drawRect(Color color, SDL_Rect &rect);
    void drawLine(Color color, int x1, int y1, int x2, int y2);
    void renderClearBlack();
    void setTitle(const std::string &title);

    void resize(Uint32 w, Uint32 h) override;
};
#endif //MYTEMPLATEPROJECT_WINDOW_H
