/**
 * @file Drawer.cpp
 * @author jmmut
 * @date 2019-03-17.
 */

#include "Drawer.h"

int quantize(double value);

Drawer::Drawer(Window2D &window2D) : window2D(window2D) {}

void Drawer::drawField(const Field &field, SDL_Rect fieldFrame) {
    window2D.drawRect(BORDER_COLOR, fieldFrame);
    for (const auto &bot : field.getBots()) {
        drawBot(bot, fieldFrame);
    }
    for (const auto &bot : field.getPredatorBots()) {
        drawPredatorBot(bot, fieldFrame);
    }
    for (const auto &food : field.getFoods()) {
        drawFood(food, fieldFrame);
    }
}

void Drawer::drawBot(BotEntity bot, SDL_Rect fieldFrame) {
    SDL_Rect rect = getRect(bot, fieldFrame);
    auto red = Uint8(20 + 235 * bot.getPain());
    auto green = Uint8(255 * bot.getEnergyRatio());
    auto blue = Uint8(255 * bot.getActivation());
    window2D.fillRect(Color{red, green, blue, 255}, rect);
    make1PixelBigger(rect);
    make1PixelBigger(rect);

    window2D.drawRect(BOT_COLOR, rect);
    make1PixelBigger(rect);
    window2D.drawRect(BOT_COLOR, rect);
}

void Drawer::drawPredatorBot(PredatorBotEntity bot, SDL_Rect fieldFrame) {
    SDL_Rect rect = getRect(bot, fieldFrame);
    auto red = Uint8(20 + 235 *bot.getPain());
    auto green = Uint8(255 * bot.getEnergyRatio());
    auto blue = Uint8(255 * bot.getActivation());
    window2D.fillRect(Color{red, green, blue, 255}, rect);
    make1PixelBigger(rect);
    make1PixelBigger(rect);
    window2D.drawRect(PREDATOR_COLOR, rect);
    make1PixelBigger(rect);
    window2D.drawRect(PREDATOR_COLOR, rect);
}

void Drawer::make1PixelBigger(SDL_Rect &rect) const {
    rect.x -= 1;
    rect.y -= 1;
    rect.w += 2;
    rect.h += 2;
}

SDL_Rect Drawer::getRect(const LocatedEntity &entity, const SDL_Rect &fieldFrame) const {
    auto position = entity.getPosition();
    auto size = entity.getSize();
    return getRect(fieldFrame, position, size);
}

SDL_Rect Drawer::getRect(const SDL_Rect &fieldFrame, Position positionWithinFrame, Size size) const {
    SDL_Rect rect{
            quantize(positionWithinFrame.first + fieldFrame.x),
            quantize(positionWithinFrame.second + fieldFrame.y),
            quantize(size.first),
            quantize(size.second)};
    return rect;
}

int quantize(double value) {
    return int(std::floor(value));
}

void Drawer::drawBotStats(BotEntity bot, SDL_Rect statsFrame) {
    auto columnPad = statsFrame.w * 0.25;
    auto columnWidth = int(columnPad * 0.5);

    auto redHeight = int(statsFrame.h * bot.getPain());
    auto redRect = SDL_Rect{int(statsFrame.x + columnPad * 1), statsFrame.h - redHeight, columnWidth, redHeight};
    window2D.fillRect(Color{255, 0, 0, 255}, redRect);

    auto greenHeight = int(statsFrame.h * bot.getEnergyRatio());
    auto greenRect = SDL_Rect{int(statsFrame.x + columnPad * 2), statsFrame.h - greenHeight, columnWidth, greenHeight};
    window2D.fillRect(Color{0, 255, 0, 255}, greenRect);

    auto blueHeight = int(statsFrame.h * bot.getActivation());
    auto blueRect = SDL_Rect{int(statsFrame.x + columnPad * 3), statsFrame.h - blueHeight, columnWidth, blueHeight};
    window2D.fillRect(Color{0, 0, 255, 255}, blueRect);
}

void Drawer::drawPopulationStats(const std::vector<std::pair<std::deque<long>, Color>> &populations,
        SDL_Rect populationFrame, double scaleUnitsToPixels, double scaleTimeToPixels) {
    for (const auto &population : populations) {
        auto samples = std::min(
                population.first.size(),
                static_cast<std::size_t>(populationFrame.w / scaleTimeToPixels));

        for (size_t i = 0; i < samples; ++i) {
            auto locationX = populationFrame.w - 1 - i * scaleTimeToPixels;
            auto locationY = populationFrame.h - std::min(
                    static_cast<long>(scaleUnitsToPixels * log2(population.first[i])),
                    static_cast<long>(populationFrame.h));
            auto color = population.second;
            auto rect = getRect(populationFrame, {locationX, locationY}, {1, 1});
            window2D.fillRect(color, rect);
        }
    }
}

void Drawer::drawFood(FoodEntity foodEntity, SDL_Rect fieldFrame) {
    SDL_Rect rect = getRect(foodEntity, fieldFrame);
    Color foodColor = FOOD_COLOR;
    foodColor.green = Uint8(FOOD_COLOR.green * foodEntity.getEnergyRatio());
    window2D.fillRect(foodColor, rect);
}
