/**
 * @file Window2D.cpp
 * @author jmmut
 * @date 2018-05-27.
 */

#include <algorithm>
#include <complex>
#include "Window2D.h"
#include "utils/exception/StackTracedException.h"
#include "utils/time/TimeRecorder.h"

using randomize::utils::exception::StackTracedException;
using namespace std::string_literals;

void Window2D::open() {
    WindowRenderer::open();
    if (renderer == nullptr) {
        throw StackTracedException("renderer was null. inner message: "s + SDL_GetError());
    }
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
    SDL_SetRenderTarget(renderer, NULL);
    renderClearBlack();
}

void Window2D::fillRect(Color color, SDL_Rect &rect) {
    SDL_SetRenderDrawColor(renderer, color.red, color.green, color.blue, color.alpha);
    SDL_RenderFillRect(renderer, &rect);
}

void Window2D::drawRect(Color color, SDL_Rect &rect) {
    SDL_SetRenderDrawColor(renderer, color.red, color.green, color.blue, color.alpha);
    SDL_RenderDrawRect(renderer, &rect); // only outline
}

void Window2D::drawLine(Color color, int x1, int y1, int x2, int y2) {
    SDL_SetRenderDrawColor(renderer, color.red, color.green, color.blue, color.alpha);
    SDL_RenderDrawLine(renderer, x1, y1, x2, y2);
}

void Window2D::renderClearBlack() {
    SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0x00);
    WindowRenderer::renderClear();
}

void Window2D::setTitle(const std::string &title) {
    SDL_SetWindowTitle(window, title.c_str());
}

void Window2D::resize(Uint32 w, Uint32 h) {
    Window::resize(w, h);
    renderClearBlack();
}
