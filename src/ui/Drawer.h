/**
 * @file Drawer.h
 * @author jmmut
 * @date 2019-03-17.
 */

#ifndef EMOTIONBOTS_DRAWER_H
#define EMOTIONBOTS_DRAWER_H


#include <deque>
#include <field/Field.h>
#include "field/BotEntity.h"
#include "Window2D.h"
#include "SpaceConverter.h"


//static
const Color BOT_COLOR{100, 200, 255, 255};

static const Color PREDATOR_COLOR{255, 0, 0, 255};

static const Color FOOD_COLOR{40, 255, 40, 255};

static const Color BORDER_COLOR{50, 50, 250, 255};

class Drawer {
public:
    Drawer(Window2D &window2D);

    void drawField(const Field &field, SDL_Rect fieldFrame);
    void drawBotStats(BotEntity bot, SDL_Rect statsFrame);
    void drawPopulationStats(const std::vector<std::pair<std::deque<long>, Color>> &populations,
            SDL_Rect populationFrame, double scaleUnitsToPixels, double scaleTimeToPixels);
    void drawBot(BotEntity bot, SDL_Rect fieldFrame);
    void drawPredatorBot(PredatorBotEntity bot, SDL_Rect fieldFrame);
    void drawFood(FoodEntity foodEntity, SDL_Rect fieldFrame);

    template <typename BOTS>
    void drawPersonalityStats(const BOTS &bots, SDL_Rect personalityFrame) {
        window2D.drawLine(BORDER_COLOR,
                int(personalityFrame.x + personalityFrame.w / 2.0), personalityFrame.y,
                int(personalityFrame.x + personalityFrame.w / 2.0), personalityFrame.y + personalityFrame.h);
        window2D.drawLine(BORDER_COLOR,
                personalityFrame.x, int(personalityFrame.y + personalityFrame.h / 2.0),
                personalityFrame.x + personalityFrame.w, int(personalityFrame.y + personalityFrame.h / 2.0));

        PersonalityConverter converter;
        for (const Bot &bot : bots) {
            auto personality = bot.getPersonality();
            auto pixelPosition = converter.toPixel(personalityFrame,
                    {personality.painSensitivity, personality.explorationMotivation});
            auto rect = SDL_Rect{int(pixelPosition.first), int(pixelPosition.second), 2, 2};
            window2D.drawRect(Color{255, 255, 255, 25}, rect);
        }
    }
private:
    Window2D &window2D;

    SDL_Rect getRect(const LocatedEntity &entity, const SDL_Rect &fieldFrame) const;
    SDL_Rect getRect(const SDL_Rect &fieldFrame, Position positionWithinFrame, Size size) const;
    void make1PixelBigger(SDL_Rect &rect) const;
};



#endif //EMOTIONBOTS_DRAWER_H
