/**
 * @file food.test.cpp
 * @author jmmut
 * @date 2019-03-21.
 */
#include <test/TestSuite.h>
#include <field/FoodEntity.h>
#include <field/BotEntity.h>
#include <field/actions.h>

bool floatEquals(double value, double expected);

using randomize::test::TestSuite;

int main() {
    TestSuite{"food", {
            []() {
                FoodEntity food{5, 0, 0, 0, 0};
                BotEntity bot{100, 0, 0, 0, 0};
                eat(bot, food, 1);
                ASSERT_EQUALS_OR_THROW(bot.getEnergy(), 100);
                ASSERT_OR_THROW(floatEquals(food.getEnergy(), 5 - EATING_SPEED_IN_ENERGY_UNITS_PER_MILLISECOND));
            }, []() {
                FoodEntity food{5, 0, 0, 0, 0};
                BotEntity bot{100, 0, 0, 0, 0};
                eat(bot, food, 10);
                ASSERT_EQUALS_OR_THROW(bot.getEnergy(), 100);
                ASSERT_OR_THROW(floatEquals(food.getEnergy(), 5 - 10 * EATING_SPEED_IN_ENERGY_UNITS_PER_MILLISECOND));
            }, []() {
                FoodEntity food{5, 0, 0, 0, 0};
                BotEntity bot{100, 0, 0, 0, 0};
                eat(bot, food, 1);
                ASSERT_EQUALS_OR_THROW(bot.getEnergy(), 100);
                ASSERT_OR_THROW(floatEquals(food.getEnergy(), 5 - EATING_SPEED_IN_ENERGY_UNITS_PER_MILLISECOND));
                bot.age(1000);

                auto oldEnergy = bot.getEnergy();
                eat(bot, food, 1);
                ASSERT_EQUALS_OR_THROW(bot.getEnergy(), oldEnergy + EATING_SPEED_IN_ENERGY_UNITS_PER_MILLISECOND);
                ASSERT_OR_THROW(floatEquals(food.getEnergy(), 5 - 2 * EATING_SPEED_IN_ENERGY_UNITS_PER_MILLISECOND));
            }
    }};

    return 0;
}

bool floatEquals(double value, double expected) {
    return std::abs(value - expected) < 0.0000001;
}
